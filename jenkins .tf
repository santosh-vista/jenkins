

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8080"
        to_port     = "8080"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "jbos" {
    ami                         =  var.jbos
    instance_type               = "t2.large"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.jbos.public_ip
    }

     provisioner "remote-exec" {
    inline = [
   
"sudo apt update -y",
 "sudo apt install default-jre -y",
"wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -",
"sudo echo deb http://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list",
"sudo apt update -y",
"sudo apt install jenkins -y ",
"sudo systemctl start jenkins",
]
  }
   tags = {
    Name = "jbos"
  }
}